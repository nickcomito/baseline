$(document).ready(function() {
	if (window.innerWidth < 640 ) {
		$("#primary-menu").addClass("left-off-canvas-menu");
		$("#primary-menu").removeClass("primary-nav");
	}
	else {

		$("#primary-menu").removeClass("left-off-canvas-menu");
		$("#primary-menu").addClass("primary-nav");
	}
});